from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.views.generic import TemplateView
admin.autodiscover()

urlpatterns = patterns('webadm',
    url(r'^$', 'views.home', name='home'),
    url(r'^logout/$', 'views.account_logout', name='logout_web'),
    url(r'^list_users/', 'views.list_users', name='list_users'),
    url(r'^create_user/', 'views.create_user', name='create_account'),
    url(r'^edit_user/(?P<user_id>\d+)/$', 'views.edit_user', name='edit_user'),
    url(r'^delete_user/(?P<user_id>\d+)/$', 'views.delete_user', name='delete_user'),
    url(r'^change-password/(?P<user_id>\d+)/$', 'views.change_password', name='change_password'),
)
