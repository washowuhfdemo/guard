from django.shortcuts import render, redirect, render_to_response
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User, Group
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.template import RequestContext
from django.db.models import Q, Count, Max, Sum
from django_user_agents.utils import get_user_agent
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.validators import URLValidator, validate_email
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.contrib.auth.forms import SetPasswordForm, AdminPasswordChangeForm
from registration.models import RegistrationProfile
from webadm.forms import UserCreateForm, UserProfileFormSet

import random
import datetime
from datetime import timedelta
import time
import json
import urllib2
from time import strptime, strftime

URL_RENDER = {
    'list_users': 'webadm/list_users.html',
}

def washow_tag_validation_url():
    return 'http://service.washow.cc/v1/api/developer/validate_tag/'

def is_member(user):
    return user.groups.filter(name='webadm').exists()

"""
Major Django views

"""
def home(request):
    if request.user.is_authenticated():
        if is_member(request.user):
            return redirect(reverse(list_users), locals(), request)
        else:
            return render_to_response('webadm/no_permission.html')
    else:
        return render_to_response('webadm/index.html')

def account_logout(request):
    logout(request)
    return render_to_response('webadm/index.html')

@login_required(login_url='/webadm/')
def list_users(request):
    try:
        users = User.objects.filter(Q(is_staff=False)|Q(is_superuser=False)).exclude(username=request.user).order_by('id')
    except ObjectDoesNotExist:
        users = None
    return render(request, URL_RENDER['list_users'], locals())

@login_required(login_url='/webadm/')
def create_user(request):
    if request.method == 'POST':
        form = UserCreateForm(request.POST)
        if form.is_valid():
            user = form.save()
            return redirect(reverse(list_users), locals(), request)
    else:
        form = UserCreateForm()
        formset = UserProfileFormSet(instance=User())

    return render_to_response('webadm/register.html', {'form': form, 
        }, context_instance=RequestContext(request))

         
@login_required(login_url='/webadm/')
def edit_user(request):
    pass

@login_required(login_url='/webadm/')
def delete_user(request, user_id):
    try:
        user = User.objects.get(id=user_id)
        if user.id == 1:
            pass
        else:
            user.delete()
    except ObjectDoesNotExist:
        pass
    return redirect(reverse(list_users))

@login_required(login_url='/webadm/')
def change_password(request, user_id):
    user = User.objects.get(pk=user_id)
    form = AdminPasswordChangeForm(user, request.POST)
    if form.is_valid():
        new_user = form.save()
        return render_to_response('webadm/password_change_done.html')
    else:
        form = AdminPasswordChangeForm(user)
    extra_context = {'form': form,
        'change': True
        }
    return render_to_response('webadm/password_change_form.html', {'form': form, 'change': True
        }, context_instance=RequestContext(request))
