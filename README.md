# README #

Guard Service:

-UHF Guard
  1. Registration/ Admin
	.Reader: UUID /M_Key /M_Name /M_Location /Note
	.Tag: TID/ EPC_code / T_Name /Note
	. Area Definition: List of M_Name
  
  2. ATTN: Reader check-in every hours
	. GET /Key&UUID ;SEND STD_DateTime, Search_EPC, MSG

  3. LOG: Reader Send LOG
         . POST /EPC_code, Loc_DateTime, Status

  4. Area Monitor 
	. Area Manager Account Login
	. Set Search_EPC with option=Local, All Area

API:
—上傳TAG資料 ex: http://guard.washow.cc/api/reader/12345678/epc/1111/

POST /api/reader/Ｍ_ID/epc/T_UID/   ; Ｍ_ID=12345678, T_UID=1111

Host: guard.washow.cc

Content-Type: application/json

Authorization: token f65c42d009b8b44a36ef494d7814258f0fef9b76

Cache-Control: no-cache

Content-Length: 48

{"dev_created":"1970-07-19T12:20","status":"FF"}

———————————————

{
  "id": 280,
  "epc_code": "1111",
  "reader": "123456789",
  "dev_created": "1970-07-19T12:20:00",
  "status": "00"
}

=======================

—Reader報到 ex: http://guard.washow.cc/api/reader/12345678/

POST /api/reader/Ｍ_ID/epc/      ; Ｍ_ID=12345678

Host: guard.washow.cc

Content-Type: application/json

Authorization: token f65c42d009b8b44a36ef494d7814258f0fef9b76

Cache-Control: no-cache

Content-Length: 10

{"":""}

——————

{
  "epc_code": "",
  "sys_msg": 999,
  "time": "2016-08-04 08:58:22"
}

==========================