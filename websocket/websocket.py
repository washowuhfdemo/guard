import requests

WS_HOST = 'http://localhost:3006'

def send_notify(data):
    requests.post(WS_HOST + '/notify-all/', json={'json': data.serialize()})

