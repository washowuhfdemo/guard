var EVT;

EVT = {
    update: 'update',
    disconnectRequest: 'disconnect-request',
};

function log(msg) {
    console.log(msg);
}

function IoClient(socket, token, callbacks) {
    var that = this;
    this.socket = socket;
    this.token = token;
    this.socket.on('disconnect', function() {
        callbacks.onDisconnect(socket);
    });
}

IoClient.prototype.notifyClient = function(data) {
    this.socket.emit(EVT.update, data);
}

IoClient.prototype.disconnectRequest = function() {
    this.socket.emit(EVT.disconnectRequest);
}

module.exports = (function() {
    var clients = [];

    function removeClient(socket) {
        var i;
        for (i = 0; i < clients.length; i++) {
            if (clients[i].socket === socket) {
                clients.splice(i, 1);
                return;
            }
        }
    }
       
    return {
        addClient: function(socket) {
            var client,
                token = socket.handshake.query.token;
            console.log('token: ' + token);
            client = new IoClient(socket, token, {
                onDisconnect: function(socket) {
                    log('disconnected');
                    removeClient(socket);
                }
            });
            clients.push(client);
        },

        notifyUpdate: function(tokens) {
            clients.filter(function(c) {
                return tokens.indexOf(c.token) > -1;
            }).forEach(function(c) {
                c.notifyClient();
            });
        },

        notifyAll: function(data) {
            clients.forEach(function(c) {
                c.notifyClient(data);
            });
        },

        disconnectRequests: function(tokens) {
            clients.filter(function(c) {
                return tokens.indexOf(c.token) > -1;
            }).forEach(function(c) {
                c.disconnectRequest();
            });
        },
    };
}());
