var express = require('express'),
    app = express(),
    http = require('http').createServer(app),
    io = require('socket.io')(http),
    morgan = require('morgan'),
    bodyParser = require('body-parser'),
    ioClients = require('./io-clients'),
    config = require('./config');

function log(msg) {
    console.log(msg);
}
    
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
    
/**
 * ------------------------------------------------
 *  RESTful API
 * ------------------------------------------------
 */
app.post('/notify-all/', function(req, res, next) {
    ioClients.notifyAll(req.body.json);
    res.json({
        result: 0,
    });
});

app.post('/notify-update/', function(req, res, next) {
    var tokens = req.body.tokens;
    if (tokens.length > 0) {
        log('Update ' + tokens.length + ' clients');
        ioClients.notifyUpdate(tokens);
        res.json({
            result: 0
        });
    } 
});

app.post('/disconnect-request/', function(req, res, next) {
    var tokens = req.body.tokens;
    if (tokens.length > 0) {
        log('Disconnect ' + tokens.length + ' clients');
        ioClients.disconnectRequests(tokens);
        res.json({
            result: 0
        });
    } 
});

app.get('/version', function(req, res, next) {
    res.json({
        result: 0,
        version: config.version,
    });
});

app.use(express.static('./public'));

/**
 * ------------------------------------------------
 *  SOCKET.IO
 * ------------------------------------------------
 */

io.on('connection', function(socket) {
    console.log('new connection');
    ioClients.addClient(socket);
});

http.listen(config.port, '0.0.0.0');

