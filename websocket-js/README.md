## Tessuto state change listener

Admin subscribe to all updates, and client subscribe to some room's update.
* these parts are implemented using socket.io

the update is sent using RESTful API:
* POST to /update/:roomId

### Issues:

* app.listen() vs http.listen(), the differences?
* socket.io-client: io('http://localhost:3003') vs io('http://127.0.0.1:3003')?
