var IoClient = function(url, callbacks) {
    var socket, 
        EVT = {
            update: 'update',
            disconnectRequest: 'disconnect-request',
        };

    return {
        connect: function(token) {
            if (socket) 
                return;
            socket = io(url, {query: 'token=' + token});
            socket.on(EVT.update, function(data) {
                callbacks.onUpdate(data);
            });
            socket.on(EVT.disconnectRequest, function(data) {
                socket.disconnect();
                callbacks.onDisconnected(data);
            });
        }
    };
};
