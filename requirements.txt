Django==1.9.2
django-cors-headers==1.1.0
django-geoposition==0.3.0
django-registration-redux==1.4
djangorestframework==3.4.6
djangorestframework-jwt==1.8.0
MySQL-python==1.2.5
PyJWT==1.4.2
requests==2.12.4
rfc3339==5
