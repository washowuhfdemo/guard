import string
import random
import datetime
from django.db import models
from django.contrib.auth.models import User, BaseUserManager, AbstractUser
from django.contrib import admin
from django.utils import timezone
from geoposition.fields import GeopositionField

import rfc3339

def KeyGenerator():
    character = string.lowercase + string.uppercase + string.digits
    char_len = len(character)
    # you can specify your key length here
    genkey_len = random.randint(6,10)
    genkey = ''
    for x in range(genkey_len):
        genkey = genkey + character[random.randint(0,char_len-1)]
    return genkey

class Area(models.Model):
    user = models.ForeignKey(User)
    name = models.CharField(max_length=32)
    description = models.CharField(max_length=64)
    created = models.DateTimeField(default=timezone.now)
    modified = models.DateTimeField(default=timezone.now)
    def __unicode__(self):
        return self.name

    class Meta:
        db_table = "area"

    def serialize(self):
        return {
            "user": self.user.username,
            "name": self.name,
            "description": None if self.description is None else self.description,
            "created": rfc3339.rfc3339(self.created),
            "modified": rfc3339.rfc3339(self.modified)
        }

class AreaAdmin(admin.ModelAdmin):
    pass

class Reader(models.Model):
    area = models.ForeignKey(Area, related_name="reader")
    name = models.CharField(max_length=64)
    uuid = models.CharField(max_length=16)
    key = models.CharField(max_length=64)
    location = models.CharField(max_length=80)
    note = models.CharField(max_length=256)
    created = models.DateTimeField(default=timezone.now)
    status = models.BooleanField(default=True)
    def __unicode__(self):
        return self.name

    class Meta:
        db_table = "reader"

    def serialize(self):
        return {
            "area": self.area.name,
            "name": self.name,
            "uuid": self.uuid,
            "key": self.key,
            "location": self.location,
            "note": None if self.note is None else self.note,
            "created": rfc3339.rfc3339(self.created),
            "status": self.status
        }


class ReaderAdmin(admin.ModelAdmin):
    pass

class Tag(models.Model):
    user = models.ForeignKey(User)
    uid = models.CharField(max_length=16)
    epc_code = models.CharField(max_length=16)
    name = models.CharField(max_length=64)
    created = models.DateTimeField(default=timezone.now)
    modified = models.DateTimeField(default=timezone.now)
    status = models.BooleanField(default=True)
    note = models.CharField(max_length=256)
    def __unicode__(self):
        return self.uid

    class Meta:
        db_table = "tag"

    def serialize(self):
        return {
            "user": self.user.username,
            "uid": self.uid,
            "epc_code": self.epc_code,
            "name": self.name,
            "note": None if self.note is None else self.note,
            "created": rfc3339.rfc3339(self.created),
            "modified": rfc3339.rfc3339(self.modified),
            "status": self.status
        }

class TagAdmin(admin.ModelAdmin):
    pass

class Alarm(models.Model):
    tag = models.ForeignKey(Tag)
    reader = models.CharField(max_length=64, blank=True)
    status = models.BooleanField(default=True)
    created = models.DateTimeField(default=timezone.now)
    def __unicode__(self):
        name = self.reader
        try:
            tag_name = self.tag.epc_code
            name += ' '+tag_name
        except:
            pass
        return name

    class Meta:
        db_table = "alarm"

    def serialize(self):
        return {
            "tag": self.tag.user.username,
            "reader": self.reader,
            "created": rfc3339.rfc3339(self.created),
            "status": self.status
        }

class AlarmAdmin(admin.ModelAdmin):
     pass

class ReaderReturnLog(models.Model):
    uuid = models.CharField(max_length=16)
    created = models.DateTimeField(default=timezone.now)

    def __unicode__(self):
        return self.uuid

    class Meta:
        db_table = "readerreturnlog"

    def serialize(self):
        return {
            "uuid": self.uuid,
            "created": rfc3339.rfc3339(self.created)
        }

class ReaderReturnLogAdmin(admin.ModelAdmin):
     pass

class TagReturnLog(models.Model):
    epc_code = models.CharField(max_length=16)
    reader = models.CharField(max_length=16)
    dev_created = models.DateTimeField()
    sys_created = models.DateTimeField(default=timezone.now)
    status = models.CharField(max_length=32)
    def __unicode__(self):
        return self.epc_code

    class Meta:
        db_table = "tagreturnlog"

    def serialize(self):
        return {
            "epc_code": self.epc_code,
            "reader": self.reader,
            "dev_created": None if self.dev_created is None else rfc3339.rfc3339(self.dev_created),
            "sys_created": None if self.sys_created is None else rfc3339.rfc3339(self.sys_created),
            "status": self.status
        }

class TagReturnLogAdmin(admin.ModelAdmin):
    pass

class UserRole(models.Model):
    name = models.CharField(max_length=30)
    group_limit = models.IntegerField(default=5)
    tag_limit = models.IntegerField(default=20)

    def __unicode__(self):
        return self.name

    class Meta:
        db_table = "userrole"

class UserRoleAdmin(admin.ModelAdmin):
    pass

class UserProfile(models.Model):
    user = models.ForeignKey(User)
    role = models.ForeignKey(UserRole)
    address = models.CharField(max_length=200, blank=True, null=True)
    postcode = models.CharField(max_length=5, blank=True, null=True)
    tel = models.CharField(max_length=20, blank=True, null=True)
    mobile = models.CharField(max_length=20, blank=True, null=True)
    expired_date = models.DateField(auto_now=False)

    def __unicode__(self):
        return self.user.username

    class Meta:
        db_table = "userprofile"

class UserProfileAdmin(admin.ModelAdmin):
    pass

admin.site.register(Area, AreaAdmin)
admin.site.register(Reader, ReaderAdmin)
admin.site.register(Tag, TagAdmin)
admin.site.register(Alarm, AlarmAdmin)
admin.site.register(ReaderReturnLog, ReaderReturnLogAdmin)
admin.site.register(TagReturnLog, TagReturnLogAdmin)

from . import receivers
