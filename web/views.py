from django.shortcuts import render, redirect, render_to_response
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.template import RequestContext
from django.db.models import Q, Count, Max, Sum
from django_user_agents.utils import get_user_agent
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.validators import URLValidator, validate_email
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from registration.models import RegistrationProfile

from web.models import Area, Reader, Tag
import random
import datetime
import time
import json
import urllib2
from time import strptime, strftime

URL_RENDER = {
    'list_areas': 'web/list_areas.html',
    'list_readers': 'web/list_readers.html',
}

"""
Common function
"""
def get_date(dateFormat="%d-%m-%Y", addDays=0):
    timeNow = datetime.datetime.now()
    if (addDays!=0):
        anotherTime = timeNow + datetime.timedelta(days=addDays)
    else:
        anotherTime = timeNow
    return anotherTime.strftime(dateFormat)

def KeyGenerator():
    character = string.lowercase + string.uppercase + string.digits
    char_len = len(character)
    genkey_len = random.randint(6,10)
    genkey = ''
    for x in range(genkey_len):
        genkey = genkey + character[random.randint(0,char_len-1)]
    return genkey

"""
Report views
"""
def index(request):
    if request.user.is_authenticated():
        return redirect(reverse(list_areas), locals(), request)
    else:
        return render_to_response('web/index.html')
 
def account_logout(request):
    logout(request)
    return redirect(reverse(index))

"""
List Area by ID
"""
@login_required(login_url='/web/')
def list_areas(request):
    try:
        areas = Area.objects.filter(user=request.user).order_by('id')
    except Area.DoesNotExist:
        areas = None

    area_total = areas.count()
    
    return render(request, URL_RENDER['list_areas'], locals())

"""
List Reader by Area
"""
@login_required(login_url='/web/')
def list_readers(request, area_id):
    try:
        readers = Reader.objects.filter(area=area_id).order_by('id')
    except Reader.DoesNotExist:
        readers = None
    r_total = readers.count()

    # Enable page function, 10 readers per page.
    paginator = Paginator(readers, 10)
    page = request.GET.get('page')
    try:
        readers_p = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        readers_p = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        readers_p = paginator.page(paginator.num_pages)
    return render(request, URL_RENDER['list_readers'], locals())
