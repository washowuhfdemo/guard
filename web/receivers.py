from .models import *
from django.dispatch import receiver
from django.db.models.signals import post_save
from websocket import websocket


@receiver(post_save, sender=TagReturnLog)
def on_tag_log_saved(sender, instance=None, created=False, **kwargs):
    #alarm_flag = 7
    if int(instance.status, 16) != 0:
        websocket.send_notify(instance)
    #websocket.send_notify(instance)
    #websocket.send_notify({'test':123})


