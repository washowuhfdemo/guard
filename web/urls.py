from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic import TemplateView
admin.autodiscover()

urlpatterns = patterns('web',
    url(r'^$', 'views.index', name='index'),
    url(r'^logout/$', 'views.account_logout', name='logout_page'),
    url(r'^list_areas/', 'views.list_areas', name='list_areas'),
    url(r'^list_readers/(?P<area_id>\d+)/$', 'views.list_readers', name='list_readers'),
)
