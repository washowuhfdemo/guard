from django.conf.urls import include, url
from django.contrib import admin
from registration.forms import RegistrationForm, RegistrationFormUniqueEmail
from registration.backends.default.views import RegistrationView

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include('api.urls')),
    url(r'^web/', include('web.urls')),
    url(r'^webadm/', include('webadm.urls')),
    url(r'^accounts/', include('django.contrib.auth.urls')),
    url(r'^accounts/register/$', RegistrationView.as_view(form_class=RegistrationFormUniqueEmail), name='registration_register'),
    url(r'^accounts/', include('registration.backends.default.urls')),
]
