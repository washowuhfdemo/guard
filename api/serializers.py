import datetime
from django.utils import timezone
from django.forms import widgets
from django.contrib.auth.models import User
from rest_framework import serializers
from web.models import Area, Reader, Tag, TagReturnLog, ReaderReturnLog

class DashSerializer(serializers.Serializer):
    a_qty = serializers.IntegerField()
    r_qty = serializers.IntegerField()

class AreaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Area

class AreaDetailSerializer(serializers.ModelSerializer):
    created = serializers.HiddenField(default=timezone.now)

    class Meta:
        model = Area

class AreaAddSerializer(serializers.ModelSerializer):
    created = serializers.HiddenField(default=timezone.now)
    modified = serializers.HiddenField(default=timezone.now)

    class Meta:
        model = Area

class ReaderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Reader

class ReaderDetailSerializer(serializers.ModelSerializer):
    created = serializers.HiddenField(default=timezone.now)

    class Meta:
        model = Reader
        fields = ('id', 'name', 'uuid', 'key', 'location', 'note', 'created',)

class ReaderAddSerializer(serializers.ModelSerializer):
    created = serializers.HiddenField(default=timezone.now)

    class Meta:
        model = Reader
        fields = ('reader', 'name', 'uuid', 'key', 'location', 'note', 'created',)

class TagSerializer(serializers.ModelSerializer):

    class Meta:
        model = Tag
        fields = ('id', 'uid', 'epc_code', 'name', 'created',)

class TagDetailSerializer(serializers.ModelSerializer):
    modified = serializers.HiddenField(default=timezone.now)

    class Meta:
        model = Tag
        fields = ('id', 'uid', 'epc_code', 'name', 'modified',)

class TagAddSerializer(serializers.ModelSerializer):
    created = serializers.HiddenField(default=timezone.now)
    modified = serializers.HiddenField(default=timezone.now)
    status = serializers.HiddenField(default=True)

    class Meta:
        model = Tag

class ReaderReturnLogAddSerializer(serializers.ModelSerializer):
    created = serializers.HiddenField(default=timezone.now)

    class Meta:
        model = ReaderReturnLog

class AlarmReturnSerializer(serializers.Serializer):
    epc_code = serializers.CharField()
    now = serializers.HiddenField(default=timezone.now)
    sys_msg = serializers.CharField()

class TagReturnLogAddSerializer(serializers.ModelSerializer):
    sys_created = serializers.HiddenField(default=timezone.now)

    class Meta:
        model = TagReturnLog

