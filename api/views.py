import string
import random
import json
import datetime
from django.conf import settings
from django.db import models
from django.db.models.signals import post_save
from django.db.models import Q, Count, Max
from django.dispatch import receiver
from django.http import Http404, JsonResponse
from rest_framework.views import APIView
from rest_framework.authtoken.models import Token
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status, permissions, generics
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from web.models import Area, Reader, Tag, TagReturnLog, Alarm, ReaderReturnLog, TagReturnLog
from api.serializers import DashSerializer, AreaSerializer, AreaDetailSerializer, AreaAddSerializer
from api.serializers import ReaderSerializer, ReaderAddSerializer, ReaderDetailSerializer, TagAddSerializer, TagReturnLogAddSerializer, ReaderReturnLogAddSerializer, AlarmReturnSerializer


def console_log(message):
    import sys
    print >> sys.stderr, "\033[1;32m"+'debug: '+message+"\033[0m"


def error_log(message):
    import sys
    print >> sys.stderr, "\033[1;31m"+'debug: '+message+"\033[0m"

@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)

def KeyGenerator():
    character = string.lowercase + string.uppercase + string.digits
    char_len = len(character)
    genkey_len = random.randint(6,10)
    genkey = ''
    for x in range(genkey_len):
        genkey = genkey + character[random.randint(0,char_len-1)]
    return genkey

class GenKey(APIView):
    authentication_classes = (JSONWebTokenAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        data = {'genkey': KeyGenerator()}
        return Response(data, status=status.HTTP_200_OK)

class ReaderReturn(APIView):
    """
    Report Tag status.

    """
    #authentication_classes = (JSONWebTokenAuthentication, TokenAuthentication)
    #permission_classes = (IsAuthenticated,)
    permission_classes = ()

    def get_reader(self, id):
        try:
            return Reader.objects.get(uuid = id)
        except Reader.DoesNotExist:
            return None

    def get_alarm(self):
        try:
            return Alarm.objects.get(status=True)
        except Tag.DoesNotExist:
            return None

    def post(self, request, reader_id, format=None):
        reader = self.get_reader(reader_id)
        epc_code = self.get_alarm().tag.epc_code

        data = request.data
        if reader:
            data['uuid'] = reader_id
            serializer = ReaderReturnLogAddSerializer(data=data)
            if serializer.is_valid():
                serializer.save()
                data = {'epc_code': epc_code, 'time': datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                        'sys_msg': 999}
                return Response(data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            data = {'detail': 'Reader doesn\'t exist!'}
            return Response(data, status=status.HTTP_400_BAD_REQUEST)

class TagReturn(APIView):
    """
    Report Tag status.

    """
    #authentication_classes = (JSONWebTokenAuthentication, TokenAuthentication)
    #permission_classes = (IsAuthenticated,)
    permission_classes = ()

    def get_reader(self, id):
        try:
            return Reader.objects.get(uuid = id)
        except Reader.DoesNotExist:
            return None

    def get_tag(self, epc):
        try:
            return Tag.objects.get(epc_code = epc)
        except Tag.DoesNotExist:
            return None

    def post(self, request, reader_id, epc_code, format=None):
        reader = self.get_reader(reader_id)
        tag = self.get_tag(epc_code)

        console_log('... tag : '+str(tag))

        import sys
        try:
            data = dict(request.data)

        #import json
        #console_log()

            console_log('--- 1 ')
            if reader:
                console_log('--- 1.2')
                if tag:
                    console_log('--- 1.3')
                    data['epc_code'] = epc_code
                    console_log('--- 1.4')
                    data['reader'] = reader_id

                    console_log('--- 2')
                    serializer = TagReturnLogAddSerializer(data=data)
                    console_log('--- 3')
                    if serializer.is_valid():
                        console_log('--- 4')
                        serializer.save()
                        return Response(serializer.data, status=status.HTTP_201_CREATED)
                    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
                    console_log('--- 5')
                else:
                    data = {'detail': 'Tag doesn\'t exist!'}
                    console_log('--- 6')
                    return Response(data, status=status.HTTP_400_BAD_REQUEST)

            else:
                data = {'detail': 'Reader doesn\'t exist!'}
                return Response(data, status=status.HTTP_400_BAD_REQUEST)
        except:
            import traceback
            console_log('exception: '+traceback.format_exc())
            


class TagSearch(APIView):
    authentication_classes = (JSONWebTokenAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)
    #permission_classes = ()

    def post(self, request, reader_id, format=None):
        ReaderReturnLog.objects.create(uuid=reader_id)
        return Response({'status': 'success'})


class AlarmCreation(APIView):
    authentication_classes = (JSONWebTokenAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)
    #permission_classes = ()

    def post(self, request, tag_id, format=None):
        status_code = status.HTTP_400_BAD_REQUEST

        #if Tag.objects.filter(uid=tag_id).exists() and TagReturnLog.objects.filter(epc_code=tag_id).exists():
        if Tag.objects.filter(uid=tag_id).exists():

            tag = Tag.objects.get(uid=tag_id)

            # find the latest location that tag is discovered
            #def getNewer(log1, log2):
            #    return log1 if log1.sys_created > log2.sys_created else log2
            #reader = reduce(getNewer, TagReturnLog.objects.filter(epc_code=tag_id))

            #Alarm.objects.all().delete()
            #Alarm.objects.create(tag=tag, reader=reader)

            if len(Alarm.objects.all()) == 1:
                alarm = Alarm.objects.all()[0]
                alarm.tag = tag
                alarm.save()
                status_code = status.HTTP_201_CREATED

        return Response(status=status_code)



class AllReturn(APIView):
    """
    Report all data

    """
    #authentication_classes = (JSONWebTokenAuthentication, TokenAuthentication)
    #permission_classes = (IsAuthenticated,)
    permission_classes = ()

    def get(self, request):
        data = {}

        # areas
        all_areas = Area.objects.all()
        if len(all_areas) > 0:
            data['areas'] = [a.serialize() for a in all_areas]
        else:
            data['areas'] = []

        # readers
        all_readers = Reader.objects.all()
        if len(all_readers) > 0:
            data['readers'] = [r.serialize() for r in all_readers]
        else:
            data['readers'] = []

        # tags
        all_tags = Tag.objects.all()
        if len(all_tags) > 0:
            data['tags'] = [t.serialize() for t in all_tags]
        else:
            data['tags'] = []

        # alarms
        all_alarms = Alarm.objects.all()
        if len(all_alarms) > 0:
            data['alarms'] = [a.serialize() for a in all_alarms]
        else:
            data['alarms'] = []

        # reader return logs
        all_reader_logs = ReaderReturnLog.objects.all()
        if len(all_reader_logs) > 0:
            data['reader_return_log'] = [r.serialize() for r in all_reader_logs]
        else:
            data['reader_return_log'] = []

        # tag return logs
        all_tag_logs = TagReturnLog.objects.all()
        if len(all_tag_logs) > 0:
            data['tag_return_log'] = [t.serialize() for t in all_tag_logs]
        else:
            data['tag_return_log'] = []

        return Response(data)
