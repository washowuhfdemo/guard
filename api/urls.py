from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from api import views

urlpatterns = format_suffix_patterns([
    url(r'^api-token-auth/', 'rest_framework_jwt.views.obtain_jwt_token'),
    url(r'^api-token-refresh/', 'rest_framework_jwt.views.refresh_jwt_token'),
    url(r'^api-token-verify/', 'rest_framework_jwt.views.verify_jwt_token'),
    url(r'^reader/(?P<reader_id>\w+)/epc/(?P<epc_code>\w+)/$', views.TagReturn.as_view()),
    url(r'^reader/(?P<reader_id>\w+)/$', views.ReaderReturn.as_view()),

    url(r'^alldata/$', views.AllReturn.as_view()),
    url(r'^tag_search_record/(?P<reader_id>\w+)/$', views.TagSearch.as_view()),
    url(r'^alarm_creation/(?P<tag_id>\w+)/$', views.AlarmCreation.as_view()),
])

urlpatterns += [
    url(r'^api-auth/', include('rest_framework.urls',
                               namespace='rest_framework')),
]
