let fetch = require('node-fetch'),
    httpProxy = {
        get(url) {
            let options = {
                method: 'GET',
                headers: this.getHeaders(),
            };
            console.log('(api)(get)' + url);
            console.log('-----------------------------------------');
            return fetch(url, options);
        },

        post(url, data) {
            let options = {
                method: 'POST',
                headers: this.getHeaders(),
                body: JSON.stringify(data)
            };
            options.headers['Accept'] = 'application/json';
            options.headers['Content-Type'] = 'application/json';
            console.log('(api)(post)' + url);
            console.log('-----------------------------------------');
            console.log('(request)');
            console.log(JSON.stringify(data, null, 2));
            return fetch(url, options);
        },

        put(url, data) {
            let options = {
                method: 'PUT',
                headers: this.getHeaders(),
                body: JSON.stringify(data)
            };
            options.headers['Accept'] = 'application/json';
            options.headers['Content-Type'] = 'application/json';
            console.log('(api)(put)' + url);
            console.log('-----------------------------------------');
            console.log('(request)');
            console.log(JSON.stringify(data, null, 2));
            return fetch(url, options);
        },

        delete(url, data) {
            let options = {
                method: 'DELETE',
                headers: this.getHeaders(),
                body: JSON.stringify(data)
            };
            options.headers['Accept'] = 'application/json';
            options.headers['Content-Type'] = 'application/json';
            console.log('(api)(delete)' + url);
            console.log('-----------------------------------------');
            console.log('(request)');
            console.log(JSON.stringify(data, null, 2));
            return fetch(url, options);
        },

        setHeader(key, value) {
            if (!this.customHeaders) {
                this.customHeaders = {};
            }
            if (value == undefined) {
                delete this.customHeaders[key];
            } else {
                this.customHeaders[key] = value;
            }
        },

        getHeaders() {
            if (!this.customHeaders) {
                this.customHeaders = {};
            }
            return this.customHeaders;
        }

    };

module.exports = httpProxy;

