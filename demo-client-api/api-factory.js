var ApiFactory = {
    create: function(httpProxy, host) {
        if (ApiFactory.API === undefined) {
            ApiFactory.API = {
                /**
                 * api begin
                 */
                hello: function() {
                    return httpProxy.get(host + '/hello/');
                },

                echo: function(message) {
                    return httpProxy.post(host + '/echo/', {message: message});
                },

                setAuthToken: function(token, type='Token') {
                    httpProxy.setHeader('Authorization', type+' ' + token);
                },

                doLogin: function({username, password}) {
                    return httpProxy.post(host + '/api/api-token-auth/', {username, password});
                },

                listAllData: function() {
                    return httpProxy.get(host + '/api/alldata/');
                },

                searchTag: function(tagUUID) {
                    return httpProxy.post(host + '/api/tag_search_record/'+tagUUID+'/');
                },

                createAlarm: function(tagUUID) {
                    return httpProxy.post(host + '/api/alarm_creation/'+tagUUID+'/');
                }
            }
        }
        return ApiFactory.API;
    },
}

if (module !== undefined) {
    module.exports = ApiFactory;
}
