let ApiFactory = require('./api-factory');
let API = ApiFactory.create(require('./http-proxy'), 'http://aidialink-guard.smarli-tech.com');

function parseResponse(response) {
    return response.json().then(data => {
        return new Promise(function(resolve, reject) {
            if (data) {
                resolve(data);
            } else {
                reject('data is null')
            }
        });
    });
}

const username = 'admin',
      password = 'Smarli123';


// login and list all data (areas/reader/tag/alarm)
//API.doLogin({username: username, password: password}).then(response => parseResponse(response))
//    .then(data => {
//        API.setAuthToken(data.token, 'JWT');
//        return API.listAllData().then(response => parseResponse(response));
//    })
//    .then(data => {
//        console.log('[read all data] '+JSON.stringify(data));
//    })
//    .then(() => API.searchTag('1234').then(response => parseResponse(response)))
//    .then(data => console.log('[create reader return log] '+JSON.stringify(data)))
//    .catch(err => console.log('!!! error !'));

API.listAllData().then(response => parseResponse(response))
    .then(data => console.log('[read all data] '+JSON.stringify(data)))
    .then(() => API.createAlarm('1234').then(response => parseResponse(response)))
    .then(data => console.log('[create reader return log] '+JSON.stringify(data)))
    .catch(err => console.log('error ! '+JSON.stringify(err)));



// --------------------- //
